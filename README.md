Bopepo
======

[![Build Status](https://ci-jrimum.rhcloud.com/buildStatus/icon?job=Bopepo)](https://ci-jrimum.rhcloud.com)

Biblioteca Java para geração de boletos bancários.

 * http://jrimum.org/bopepo

Para construir as bibliotecas para a versão 0.2.4-DEV-SNAPSHOT, baixe os projetos relacionados jrimum/\*.

Execute o comando de instalação do maven, com o *goal* install: 

```bash
mvn install
```

Automaticamente a biblioteca ficará disponível no repositório local.

